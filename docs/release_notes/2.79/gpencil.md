# Grease Pencil

## Interpolation Operators

Two new operators were added for interpolating between a pair of Grease
Pencil frames:

- **Interpolate** (Shortcut **Ctrl+Alt+E**) - This is equivalent to the
  *Breakdowner* tool for armatures. It allows you to interactively pick
  an new sketch interpolate from the neighbouring sketches
- **Sequence** (Shortcut **Ctrl+Shift+E**)- This fills the space between
  a pair of Grease Pencil frames with interpolated frames. It is
  equivalent to the *Sample* tool in the Dope Sheet Editor

To use these operators, there need to be Grease Pencil frames on both
sides of the current frame. (Also, note that the current frame cannot be
on one of the frames that the interpolation is occurring between).

It is possible to control how the *Interpolate Sequence* tool blends
between the two frames using the options under *Sequence Options*. There
are 3 basic types of options:

1.  **Linear** - By default, the Sequence operator will perform Linear
    Interpolation. That is, it smoothly interpolates between the two
    frames at a constant rate.
2.  **Easing Equations** - The Robert Penner easing equations (and
    associated controls) can also be used to control the interpolation
    speed/shape, just like for F-Curves/Keyframes.
3.  **Custom Curve** - It is also possible to define a custom curve to
    get more fine-grained control over the interpolation using a
    curve-map widget.

To make it easier to fine-tune the interpolation, there's also a new
operator, **Remove Breakdowns**, which can be used to get rid of the
newly generated "breakdown" keyframes (i.e. the small blue ones).

All these operators and options can be found in the toolshelf under the
"Interpolate" panel (between **Edit Strokes** and **Sculpt Strokes**).

## Other Improvements

- **Per-layer option to always show onion skinning**. Sometimes it can
  be useful to be able to keep onion skins visible in the OpenGL renders
  and/or when doing animation playback. This option can be found as the
  small "camera" toggle between the "Use Onion Skinning" and "Use Custom
  Colors" options. There are two use cases where this is quite useful:
  - For creating a cheap motion-blur effect, especially when the
    before/after values are also animated.
  - If you've animated a shot with onion skinning enabled, the poses may
    end up looking odd if the ghosts are not shown (as you may have been
    accounting for the ghosts when making the compositions).
- **Add Blank Frame** (Shortcut **D+B**). This operator adds a new frame
  with nothing in it on the current frame. If there is already a frame
  there, all existing frames are shifted one frame later.
- **Numpad keys** now work when doing sculpt sessions.
- **Tools in 3D View menus**. Previously, many of these were only
  available in the tool shelf or pie menus.
- **Reproject Strokes option to project strokes onto geometry**, instead
  of only doing this in a planar (i.e. parallel to viewplane) way.
  (blender/blender@6d868d9f48a35a0)
