# Blender 4.2: EEVEE & Viewport

## EEVEE
The engine was rewritten to allow deeper changes, removing long standing limitations and ease its future evolution.

### Global Illumination
EEVEE now uses screen space ray tracing for every BSDF. There is no longer any limitation to the number of BSDF.

Material using `Blended` render method are now removed from reflections instead of being projected onto the background.

### Lights
There is no longer a limit to the number of lights in a scene. However, only 4096 lights can be visible at the same time.

- Lights are now visible through refractive surfaces. A new transmission influence factor has been added and set to 0 for older files.
- Lights now support ray visibility options.
- Glossy lighting no longer leaks at the back of objects.

### Shadows
Shadows are now rendered using Virtual Shadow Maps. This greatly increases the maximum resolution, reduce biases and simplify the setup.

- Light visibility is now computed using Shadow Map Ray Tracing, providing plausible soft shadows without needing shadow jittering.
- Shadow Map biases have been removed and computed automatically. A workaround for the shadow terminator issue is under development.
- Contact shadows have been removed since Virtual Shadow Maps are precise enough in most cases.
- Shadow clip start has been removed and replaced by an automatic value.

### Shading
#### Shading modes
`Material > Blend Mode` has been replaced by `Material > Render Method`. `Blended` correspond to the former `Alpha Blend`.
`Material > Shadow Mode` has been replaced by `Object > Visibility > Ray Visibility > Shadow` at the object level.

A backface culling option for shadow as well as a `Transparent Shadow` option was added to reduce the performance impact of rendering Virtual Shadow Maps.

For both shadow and surface mode, the behavior of transparency is the same as with former `Alpha Hashed`. To reproduce the same behavior as former `Alpha Clip`, materials need be modified by adding `Math` node with `Greater Than` mode.

`Blended` materials now have correct rendering ordering of their opaque pixels.

The `Screen-Space Refraction` option was renamed `Raytraced Transmission` and affect all transmission BSDFs (Translucent BSDF, Sub-Surface BSSDF and Refraction BSDF).

#### Displacement
Displacement is now supported with the exception of the `Displacement Only` mode which falls back to `Displacement And Bump`.

#### Thickness
A new Thickness output has been introduced. This allows better modeling of Refraction, Sub-surface Scattering and Translucency. Some material might need adjustment to keep the same appearance. This replaces the former `Refraction Depth`. #120384

#### Sub-Surface Scattering
The new Sub-surface Scattering implementation now supports any number of BSSDF nodes with arbitrary radii.
Sub-surface Scattering now doesn't leak between objects and have no energy loss.

The subsurface translucency is now always computed and the associated option has been removed.

### Light-Probes
The render panel options have been split between the light probe data panel and the world data panel.

#### Volume Light-Probes
The new volume light probes baking that converges faster, with higher resolution.

Baking is now done at the object level, allowing volume light probes to be edited after baking and linked from other files. Optionally they can bake sky visibility to allow dynamic sky lighting.

There is a new rejection algorithm and flood fill to reduce shadow leaking.

Volume light probes now affect volumetric. However object volumes and world volumes are not yet captured by the volume light probes.

#### Sphere Light-Probes
Sphere light probes are now dynamic and are updated as they moved.
Filtered version for rough surfaces are now firefly free. The look of the reflections can appear less "foggy" than in the previous version of EEVEE.

### Volume
World volume now completely block distant light (world and sun lights). Older file can be converted using the conversion operator in the help menu or in the `World > Volume` panel. #114062

- Volume lighting is now dithered to avoid flickering.
- EEVEE now maximizes the depth range automatically if no world is present.
- Mesh objects now have correct volume intersection instead of rendering bounding boxes.

### World
Sun light contribution is now automatically extracted from HDRI lighting.

### Image stability
Viewport image stability has been improved using velocity aware temporal super sampling.

Clamping options have seen their behavior unified with Cycles. They are still independant settings.

### Motion Blur
Motion blur is now partially supported in the viewport, through the camera view.

Added support for Shutter Curve.

### Depth Of Field
Depth Of Field was rewritten and optimized, removing the `Denoise Amount` and `High Quality Slight Defocus` settings which are now always on.

### UI
The UI has been adjusted to be closer to Cycles.

## Viewport Compositor

- The compositing space is now always limited to the camera region in camera view regardless of the
  passepartout value. Which means areas outside of the camera will not be composited,
  but the result will match the final render better.
