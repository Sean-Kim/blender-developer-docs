# Blender 4.2: Sculpt, Paint, Texture

<!--
## Brushes

Brushes are now stored in asset libraries shared across projects, instead of having copies in each blend file.

The built-in brushes are in the Essentials library shipped with Blender. Brushes can be created by duplicating an existing built-in brush. Each brush is saved in its own blend file in the user asset library, with textures and images packed.

The asset shelf is the new place to select brushes, instead of the toolbar. Brushes are organized into asset catalogs, instead of being grouped by tool.

The asset shelf appears horizontally across the bottom of the 3D viewport in sculpt and paint modes. It can be configured to show a subset of catalogs, depending on the needs of the project. Alternatively, brushes can be selected in a popup from the tool settings.

### Converting Brushes

To make existing brushes available, save them in a blend file in the user asset library folder. By default this folder is `Documents/Blender/Assets`. Then in the outliner with Blender File view, right click each brush and Mark as Asset.
-->

## UI
*Sculpt* and *Weight Paint* now use the global, customizable rotation increment for corresponding Line tools when snapping is enabled. (e.g. *Line Hide* in *Sculpt* and *Gradient* in *Weight Paint*) (blender/blender@1cf0d7ca6a858b5c7a0b03a224d9e826ffa38c47).

## Sculpting

- Add *Lasso Hide* tool.
  (blender/blender@68afd225018e59d63431e02def51575bfc76f5cb).

<figure>
<video src="../../../videos/4.2_sculpt_lasso_hide.mp4"
       title="Demo of Lasso Hide tool" width="400" controls=""></video>
<figcaption>Demo of Lasso Hide tool</figcaption>
</figure>

- Add *Line Hide* tool.
  (blender/blender@6e997cc75723b0dfc4d25d8246f373d46a662905).

<figure>
<video src="../../../videos/4.2_sculpt_line_hide.mp4"
       title="Demo of Line Hide tool" width="400" controls=""></video>
<figcaption>Demo of Line Hide tool</figcaption>
</figure>

- Add *Polyline Hide* tool.
  (blender/blender@55fc1066acf69dd04f7f7b1c3af4c3f360769523).

<figure>
<video src="../../../videos/4.2_sculpt_polyline_hide.mp4"
       title="Demo of Polyline Hide tool" width="400" controls=""></video>
<figcaption>Demo of Polyline Hide tool</figcaption>
</figure>

- Add option to choose between *Fast* and *Exact* solvers for *Trim* tools.
  (blender/blender@881178895b8528fd37ef41cd7a4bc481f7b7d311).

- Add *Line Trim* tool.
  (blender/blender@d4a61647bf571cb90090e1320f4aa1156482836f).
