# Adding a Geometry Node

The best way to learn how to add a geometry node is to read through recent commits
that have done the same thing.

A good example is this recent node addition: blender/blender@48b08199d5dde4efb7c4d08342017a6c77df056c

A few changes are necessary:

1. Add the definition for the node type (`GEO_NODE_*`) in the [nodes header](https://projects.blender.org/blender/blender/src/branch/main/source/blender/blenkernel/BKE_node.hh).
  - This integer is used in some places to access the node type without its string idname
2. Add a new file with the node's implementation in the geometry nodes [folder](https://projects.blender.org/blender/blender/src/branch/main/source/blender/nodes/geometry/nodes).
  - This file initializes the node type when Blender starts. The node type contains its inputs/outputs, execute callback, and other functions.
3. Add the definition for the node in [`NOD_static_types.h`](https://projects.blender.org/blender/blender/src/branch/main/source/blender/nodes/NOD_static_types.h).
  - This defines the node type's idname, RNA struct name, and its description.
4. Add the name of the new file in [CMake](https://projects.blender.org/blender/blender/src/branch/main/source/blender/nodes/geometry/CMakeLists.txt).
5. Add the node to the add menu in [Python](https://projects.blender.org/blender/blender/src/branch/main/scripts/startup/bl_ui/node_add_menu_geometry.py).
