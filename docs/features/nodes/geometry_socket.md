# Geometry Socket

The geometry socket is used to pass around different kinds of geometry including meshes, point clouds, curves, volumes and instances.

Internally, the `GeometrySet` data structure is a container for all these geometry types. It contains geometry components (`GeometryComponent`) whereby each component corresponds to a specific geometry type (e.g. `PointCloudComponent` and `MeshComponent`).

A `GeometrySet` contains at most one geometry of each type at the top-level. However, it is possible for it to contain e.g. multiple meshes using instances.

```mermaid
graph TB
  GeometrySet --> MeshComponent & VolumeComponent & InstancesComponent
  InstancesComponent --> A[GeometrySet] & B[GeometrySet]
```

Nodes can be implemented to work on some or all geometry that is passed into them. For example, the `Set Point Radius` node only affects point-clouds in the geometry socket and ignores the rest. The `Transform Geometry` node affects all geometry.


## Different Socket Types for Geometry Types?

One question that comes up a lot is why one socket is used for all
geometry data types, instead of a separate for each socket type. There
are arguments in both directions, but in general a single socket makes
much more sense.

**Pro Multiple Socket Types**

- One knows exactly what geometry type one is working with at any point.
- Transformation of one data type to another is displayed visually.

**Pro Single Socket Type**

- Requires fewer socket types (otherwise we might need socket types for
  mesh, volume, curve, text, metaballs, point clouds, hair, grease
  pencil).
- Some nodes can work on multiple different geometry types (e.g.
  Transform). Those would have to exist multiple times or have some kind
  of dynamic input.
- The geometry node group used by a modifier can have exactly one
  output, no need for one output per type.
- When building a node group that displaces points, one probably does
  not care whether the points belong to a mesh, curve, hair or whatever.
  The group could have a point cloud input, but then all the
  connectivity data would be lost after the displacement.
- Most other node-based software uses a single socket type for this
  purpose.
- A common workflow is to first build smaller objects of various types,
  which are then joined into bigger objects and so on. These bigger
  objects are mostly only transformed and instanced. At that point there
  is no need to distinguish between different geometry types anymore.
  You wouldn't want to copy Transform nodes multiple times to transform
  all components the same way. A single Transform node in the node tree
  should be enough.

**More Thoughts**

Some of the disadvantages of having multiple socket types can be
counteracted by introducing yet another "generic geometry" socket type
that can be anything and can be used on nodes that support multiple
types. But there is more complexity to deal with: What if a node only
supports a subset of all geometry types (e.g. shrinkwrap), do we use a
different type for the different combinations? Also, how does instancing
fit into the design when there are multiple socket types? Is there a
separate "Instances" socket? What happens if you want to make those
instances "real"? The type of the instances might not be known
statically, and might change over time.

So on balance, it's better to use a single socket type, and make it
clear in the documentation and UI which data types each node supports.
