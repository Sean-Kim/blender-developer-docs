# Extensions

[Extensions Platform](https://extensions.blender.org) integration and design topics.

NOTE:
Extensions are planned to be released with Blender 4.2.
