# Blender Tools

The Blender repository has a
[tools/](https://projects.blender.org/blender/blender/src/branch/main/tools)
directory with various scripts that are not needed for building
Blender but that developers may find useful.

- Utility scripts for running checks on code for style, spelling.
- Utility scripts for code-base analysis, creating credits for eg.
- Configurations for 3rd party applications, IDE's.
- Any tools we develop specifically for working with Blender.

## Overview

- `check_source/check_spelling.py`: spell checker (*Depends on
  **enchant** Python module*)
- `config/ide`: style configuration for QtCreator and Eclipse
- `utils/make_cursor_gui.py`: simple paint program to create pixmap
  cursors for `wm_cursors.c`
- `utils/make_gl_stipple_from_xpm.py`: Converter from 32x32 XPM
  images to a GL-Stipple array, typically for `glutil.c`
- `utils_maintenance/blender_update_themes.py`: run to add new
  fields into existing XML themes
- `utils_maintenance/trailing_space_clean.py`: Strip trailing space
  and `BOM` (byte-order-mark) of source files maintained by in our
  repo
